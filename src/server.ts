import * as express from 'express';
import * as cors from 'cors';
import * as bodyParser from 'body-parser';
const bearerToken = require('express-bearer-token');
import {router} from './routes/routes'

const app = express()
    .use(cors())
    .use(bodyParser.json())
    .use(bearerToken())
    .use(router);

app.listen((process.env.PORT || 8080));