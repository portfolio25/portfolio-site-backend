import {callApi} from "./api";

const groupsUrl = 'https://gitlab.com/api/v4/groups';
const projectsByGroupUrl = (id) => 'https://gitlab.com/api/v4/groups/'+id+'/projects';

export async function getAllGroups() {
    return await callApi(groupsUrl);
}

export async function getProjectsByGroupId(groupId: string) {
    return await callApi(projectsByGroupUrl(groupId));
}