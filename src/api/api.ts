import {credentials} from "./credentials";
const fetch = require('node-fetch')

export async function callApi(apiUrl) {
    const EMPTY_CONTENT = "";
    try {
        const response = await fetch(apiUrl, credentials);
        return await response.json();
    } catch (e) {
        console.error(e);
        return EMPTY_CONTENT;
    }
}