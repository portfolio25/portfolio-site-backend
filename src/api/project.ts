import {callApi} from "./api";

const url = 'https://gitlab.com/api/v4/users/cassianojaeger/projects?simple=false&order_by=created_at&sort=desc';
const languagesUrl = (id) => {return 'https://gitlab.com/api/v4/projects/'+id+'/languages'}

export async function getAllProjects() {
    return await callApi(url);
}

export async function getProjectLanguages(projectId) {
    return await callApi(languagesUrl(projectId));
}



