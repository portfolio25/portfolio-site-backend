import {NextFunction, Request, Response, Router} from 'express';
import {getProjects} from "../services/project";
import {getGroups} from "../services/group";
export const router: Router = Router();

router.get('/project', async function (req: Request, res: Response, next: NextFunction) {
    try {
        res.send(await getProjects());
    } catch (err) {
        return next(err);
    }
});

router.get('/group', async function (req: Request, res: Response, next: NextFunction) {
    try {
        res.send(await getGroups());
    } catch (err) {
        return next(err);
    }
});