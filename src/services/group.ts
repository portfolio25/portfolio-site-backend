import {getAllGroups, getProjectsByGroupId} from "../api/groups";

export async function getGroups() {
    const groups = await getAllGroups();

    await Promise.all(groups.map(async group => {
        group.projects = await getProjectsByGroupId(group.id);
        return group;
    }));

    return groups;
}