import {getAllProjects, getProjectLanguages} from "../api/project";

const DEFAULT_IMAGE = 'https://miro.medium.com/max/3200/0*QUqE4WGF8_cC9bIl.jpg';

export async function getProjects() {
    const projects = await getAllProjects();

    await Promise.all(projects.map(async project => {
        project.avatar_url = project.avatar_url ? project.avatar_url : DEFAULT_IMAGE;
        project.language = await getProjectLanguages(project.id);

        return project;
    }));

    return projects;
}